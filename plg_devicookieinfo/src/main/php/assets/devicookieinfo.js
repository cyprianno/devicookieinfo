/**
 * JS set cookie to 1, valid 1 year 
 * 
 * @param cookieName Cookie name
 */
function setDeviCookieAccepted(cookieName) {
    var today = new Date();
    var expire = new Date();
    // one year
    expire.setTime(today.getTime() + (3600 * 1000 * 24 * 365));
    document.cookie = cookieName + "=1;expires="+expire.toGMTString()+"; path=/";
	window.location.reload();
}