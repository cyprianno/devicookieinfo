<?php
/**
 * @author		Cyprian Sniegota
 * @package	devicookieinfo
 * @copyright	Copyright (C) 2013- Deviapps. All rights reserved.
 * @license	GNU/GPLv2
 */

defined('JPATH_BASE') or die;
jimport('joomla.plugin.plugin');

/**
 * Main plugin class
 *
*/
class plgSystemDevicookieinfo extends JPlugin {

	/**
	 * Modify html after render
	 *
	 */
	public function onAfterRender()
	{
		$app = JFactory::getApplication();
		if (! $app instanceof JSite ) {
			return;
		}
		/* 		$user = JFactory::getUser();
		 if ($user->id == 0) {
		return;
		} */
		$document = JFactory::getDocument();
		//1. load parameters
		$params = $this->getParameters();
		//2. check if cookie is not set
		if((isset($_COOKIE[$params->get("cookieName")]) && $_COOKIE[$params->get("cookieName")] == "1")) {
			return;
		}
		//3. check if skipSite
		$currentPath = JFactory::getURI()->toString(array('path'));
		foreach ($params->get("skipSites") as $skipSite) {
			$skipRegexp = "/".trim(str_replace(array('/',"\r"),array('\/',''),$skipSite)).'/';
			if (preg_match($skipRegexp,$currentPath)) {
				return;
			}
		}
		$body = JResponse::getBody();
		$root = JFactory::getURI()->base(true);
		//3. source css/js files
		$assetsUrl = $root.'/plugins/system/devicookieinfo/assets/';
		$sourceHeadFiles = '<script src="'.$assetsUrl.'devicookieinfo.js'.'" type="text/javascript" ></script>'
				.' <link rel="stylesheet" href="'.$assetsUrl.'devicookieinfo.css" /> ';
		//4. add extra css/js/html parameter based
		if ($params->get('styles') != "") {
			$sourceHeadFiles .= ' <style>'.$params->get('styles').'</style>';
		}
		$sourceHeadFiles .= '</head>';
		$body = str_replace('</head>', $sourceHeadFiles, $body,$first = 1);
		// display html, disable google indexing
		$respHtml = '<div id="devicookieinfo">';
		// if fade background page
		if ($params->get('fadeSite') == 1) {
			$respHtml .= '<div class="fadeall">&nbsp;</div>';
		}
		// prepare output html
		$respHtml .= '<div class="cookieInner">';
		$respHtml .= '<div class="cookieTextWrapper">';
		$respHtml .= '<div class="cookieText">';
		$respHtml .= ''.$params->get('displayText'); //main text
		$respHtml .= '</div>'; //.cookieText
		$respHtml .= '</div>'; //.cookieTextWrapper

		$respHtml .= '<div class="cookieButtonWrapper">';
		$respHtml .= '<span class="cookieButton" onclick="setDeviCookieAccepted(\''.$params->get("cookieName").'\');">';
		$respHtml .= ''.$params->get('acceptButtonText'); //button text
		$respHtml .= '</span>'; //.cookieButton
		$respHtml .= '</div>'; //.cookieButtonWrapper

		$respHtml .= '</div>'; //.cookieInner
		$respHtml .= '</div>'; //#devicookieinfo
		// if write as javascript
		$respHtml = '<!--googleoff: index--><script>//<![CDATA['."\n".'document.write(\''
				.str_replace(array("'","\n","\r"),array("\\'","",""),$respHtml).'\');'
						."\n".' //]]></script><!--googleon: index-->';
		// /if
		$body = str_replace('</body>', $respHtml.'</body>', $body,$first = 1);
		JResponse::setBody($body);
	}

	/**
	 * prepare plugin parameters
	 */
	private function getParameters()
	{
		//params:
		$this->params->set("displayText", JText::_($this->params->get("displayText")));
		$this->params->set("acceptButtonText", JText::_($this->params->get("acceptButtonText")));
		$this->params->set("skipSites", explode("\n",$this->params->get("skipSites")));
		return $this->params;
		//	displayText editor
		//  acceptButtonText text
		//	skipSites textarea
		//	styles textarea
		//  cookieName
		//	fadeSite
	}
}
